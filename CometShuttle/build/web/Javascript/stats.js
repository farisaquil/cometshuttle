﻿function createTable() {

    var stats;
    var parser;
    var inputData = "";
    var statData = "";
    inputData = "<data><route>" + document.getElementById('routeList').value + "</route><empID>" + document.getElementById('employeeList').value + "</empID><shuttleID>" + document.getElementById('shuttleID') + "</shuttleID><startDate>" + document.getElementById('startDate').value + "</startDate><endDate> " + document.getElementById('endDate').value + " </endDate></data>";
    var URI = "http://localhost:9999/CometShuttle/webresources/getStats";
    
    $.ajax({
        type: "PUT",
        url: URI,
        contentType: "application/xml",
        data: inputData,
        dataType: "application/json",
        complete: function (msg) {
            stats = msg;
        }
    });
    

    //testing 
 
    //parse the output
    if (window.DOMParser) {
        parser = new DOMParser();
        statData = parser.parseFromString(stats, "text/xml");
    }
    else // Internet Explorer
    {
        statData = new ActiveXObject("Microsoft.XMLDOM");
        statData.async = false;
        statData.loadXML(stats);
    }


    tbl = document.getElementById('statTable');

    //clear all content 
    tbl.innerHTML = "";

    var header = tbl.createTHead();
    var row = header.insertRow(0);
    var th = row.insertCell(0);
    th.innerHTML = "<b>Number of people </b>";
    var th = row.insertCell(0);
    th.innerHTML = "<b>Shuttle ID </b>";
    var th = row.insertCell(0);
    th.innerHTML = "<b>End Milaege </b>";
    var th = row.insertCell(0);
    th.innerHTML = "<b>Start Mileage </b>";
    var th = row.insertCell(0);
    th.innerHTML = "<b> End time</b>";
    var th = row.insertCell(0);
    th.innerHTML = "<b>Start time</b>";
    var th = row.insertCell(0);
    th.innerHTML = "<b>-------Driver-------</b>";

    var drivers = statData.getElementsByTagName('fullname');
    var driverIDs = statData.getElementsByTagName('userid');
    var startTimes = statData.getElementsByTagName('startime');
    var endTimes = statData.getElementsByTagName('endtime');
    var startMileages = statData.getElementsByTagName('startmileage');
    var endMileage = statData.getElementsByTagName('endmileage');
    var shuttleIDs = statData.getElementsByTagName('shuttleid');
    var numPassengersArray = statData.getElementsByTagName('totaloccupancy');
    var routeIDs = statData.getElementsByTagName('routeid');

    var tr;
    var td;
    for (var i = 0; i <= drivers.length; i++) {
        tr = "";
        td = "";
        tr = tbl.insertRow(-1);
        td = tr.insertCell(0);
        td.appendChild(drivers[0]);
        td = tr.insertCell(1);
        td.appendChild(startTimes[0]);
        td = tr.insertCell(2);
        td.appendChild(endTimes[0]);
        td = tr.insertCell(3);
        td.appendChild(startMileages[0]);
        td = tr.insertCell(4);
        td.appendChild(endMileage[0]);
        td = tr.insertCell(5);
        td.appendChild(shuttleIDs[0]);
        td = tr.insertCell(6);
        td.appendChild(numPassengersArray[0]);
    }
}


function initialize() {
    var data = "";
    var initParser;
    /*
    var URI = "http://localhost:9999/CometShuttle/webresources/initializeStats";
    $.ajax({
        type: "GET",
        url: URI,
        dataType: "application/xml",
        complete: function (msg) {
            data = msg;
        }
    });
    */
    
    //testing
    data = "<data><routes><route>Village1</route><route>village2</route></routes><employees><employee>Ajay</employee><employee>arun</employee></employees><shuttles><shuttle>1</shuttle><shuttle>2</shuttle></shuttles></data>";
    var initData = "";
    if (window.DOMParser) {
        initParser = new DOMParser();
        initData = initParser.parseFromString(data, "text/xml");
    }
    else // Internet Explorer
    {
        initData = new ActiveXObject("Microsoft.XMLDOM");
        initData.async = false;
        initData.loadXML(data);
    }

    var routes = initData.getElementsByTagName('route');
    var employees = initData.getElementsByTagName('employee');
    var shuttles = initData.getElementsByTagName('shuttle');

    var employeeList = document.getElementById('employeeList');
    var routeList = document.getElementById('routeList');
    var shuttleList = document.getElementById('shuttleIDList');

    //employees
    for (var i = 0; i < employees.length; i++) {
        var opt = document.createElement('option');
        opt.innerHTML = employees[i].childNodes[0].nodeValue;
        employeeList.appendChild(opt);
    }

    //routes
    for (var i = 0; i < routes.length; i++) {
        var opt = document.createElement('option');
        opt.innerHTML = routes[i].childNodes[0].nodeValue;
        routeList.appendChild(opt);
    }

    //shuttles
    for (var i = 0; i < shuttles.length; i++) {
        var opt = document.createElement('option');
        opt.innerHTML = shuttles[i].childNodes[0].nodeValue;
        shuttleList.appendChild(opt);
    }

}