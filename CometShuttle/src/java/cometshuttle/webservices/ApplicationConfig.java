/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cometshuttle.webservices;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Molten_DM
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(cometshuttle.webservices.AuthenticationResource.class);
        resources.add(cometshuttle.webservices.CreateRoute.class);
        resources.add(cometshuttle.webservices.GetHeatPointsResource.class);
        resources.add(cometshuttle.webservices.GetRoutes.class);
        resources.add(cometshuttle.webservices.GetShuttleLocationResource.class);
        resources.add(cometshuttle.webservices.GetShuttles.class);
        resources.add(cometshuttle.webservices.GetStatsResource.class);
        resources.add(cometshuttle.webservices.RecordShiftResource.class);
        resources.add(cometshuttle.webservices.SetShuttleGPSResource.class);
        resources.add(cometshuttle.webservices.StartShiftResource.class);
        resources.add(cometshuttle.webservices.UpdateShuttleCapacityResource.class);
    }
    
}
