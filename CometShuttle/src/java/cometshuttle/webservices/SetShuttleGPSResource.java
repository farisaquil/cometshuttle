/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cometshuttle.webservices;

import cometshuttle.helper.LatLng2;
import cometshuttle.helper.Route;
import cometshuttle.helper.RoutePoint;
import cometshuttle.helper.Shuttle;
import cometshuttle.helper.ShuttleTracker;
import cometshuttle.helper.XMLHelper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * REST Web Service
 *
 * @author Molten_DM
 */
@Path("setShuttleGPS")
//@Path("setShuttleGPS/{varX}/{varY}")
public class SetShuttleGPSResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of SetShuttleGPSResource
     */
    public SetShuttleGPSResource() {
    }

//    /**
//     * Retrieves representation of an instance of cometshuttle.webservices.SetShuttleGPSResource
//     * @param varX
//
//     * @param varY
//     * @return an instance of java.lang.String
//     */
//    @GET
//    @Produces("application/xml")
//    public String getXml(@PathParam("varX") String varX,
//    @PathParam("varY") String varY) {
//        //TODO return proper representation object
//        System.out.println(varX);
//        return "OK";
//    }
    /**
     * Retrieves representation of an instance of
     * cometshuttle.webservices.SetShuttleGPSResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/xml")
    public String getXml() {
        //TODO return proper representation object
        String content = "<location><latitude>32</latitude><longitude>-96</longitude></location>";
        try {
            getLocationFromXML(content);
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(SetShuttleGPSResource.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            Logger.getLogger(SetShuttleGPSResource.class.getName()).log(Level.SEVERE, null, e);
        }
        //System.out.println(message);
        return "OK";
    }

    /**
     * PUT method for updating or creating an instance of SetShuttleGPSResource
     *
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/xml")
    public String putXml(String content) {
        Shuttle shuttle;
        try {
            shuttle = getLocationFromXML(content);
            shuttle.setLocation2DB();
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(SetShuttleGPSResource.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            Logger.getLogger(SetShuttleGPSResource.class.getName()).log(Level.SEVERE, null, e);
        }

        System.out.println(content);
        return "Success";
    }

    private Shuttle getLocationFromXML(String xml) throws ParserConfigurationException, SAXException, IOException, Exception {

        XMLHelper xmlHelper = new XMLHelper();
        Document rootDoc = xmlHelper.loadXML(xml);
        String latitude;
        String longitude;
        String shuttleID;
        Shuttle shuttle = new Shuttle();

        //get list of all routes
        NodeList locationList = rootDoc.getElementsByTagName("location");

        if (locationList != null) {

            Node routeNode = locationList.item(0);
            Element routeElement = (Element) routeNode;

            NodeList idList = routeElement.getElementsByTagName("shuttleid");

            shuttleID = idList.item(0).getChildNodes().item(0)
                    .getNodeValue();

            NodeList latitudeList = routeElement.getElementsByTagName("latitude");

            latitude = latitudeList.item(0).getChildNodes().item(0)
                    .getNodeValue();
            NodeList longitudeList = routeElement.getElementsByTagName("longitude");

            longitude = longitudeList.item(0).getChildNodes().item(0)
                    .getNodeValue();
            System.out.println(latitude + "  " + longitude);
            shuttle.setId(Integer.parseInt(shuttleID));
            shuttle.setLatitude(Double.parseDouble(latitude));
            shuttle.setLongitude(Double.parseDouble(longitude));

        }

        return shuttle;
    }
}
