/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cometshuttle.webservices;

import cometshuttle.helper.Shift;
import cometshuttle.helper.Shuttle;
import cometshuttle.helper.XMLHelper;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * REST Web Service
 *
 * @author Molten_DM
 */
@Path("recordShift")
public class RecordShiftResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of RecordShiftResource
     */
    public RecordShiftResource() {
    }

    /**
     * Retrieves representation of an instance of cometshuttle.webservices.RecordShiftResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/xml")
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of RecordShiftResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/xml")
    public String putXml(String content) {
        
        try {
            Shift shift = getLocationFromXML(content);
            shift.add2DB();
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(RecordShiftResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Success";
    }
    
    
    //<shift><starttime>time</starttime><endtime>time</endtime><startmileage>time</startmileage><endmileage>time</endmileage></shift>
    private Shift getLocationFromXML(String xml) throws ParserConfigurationException, SAXException, IOException {

        XMLHelper xmlHelper = new XMLHelper();
        Document rootDoc = xmlHelper.loadXML(xml);
        String endTime;
        String startTime;
        String endMileage;
        String startMileage;
        String shuttleID;
        String routeID;
        String occupancy;
        String comment;
        String userID;
        Shift shift = new Shift();

        //get list of all routes
        NodeList locationList = rootDoc.getElementsByTagName("shift");

        if (locationList != null) {

            Node routeNode = locationList.item(0);
            Element routeElement = (Element) routeNode;

            NodeList idList = routeElement.getElementsByTagName("shuttleid");
            shuttleID = idList.item(0).getChildNodes().item(0)
                    .getNodeValue();
            
            NodeList routeidList = routeElement.getElementsByTagName("routeid");
            routeID = routeidList.item(0).getChildNodes().item(0)
                    .getNodeValue();

            NodeList startList = routeElement.getElementsByTagName("starttime");
            startTime = startList.item(0).getChildNodes().item(0)
                    .getNodeValue();
            
            NodeList endList = routeElement.getElementsByTagName("endtime");
            endTime = endList.item(0).getChildNodes().item(0)
                    .getNodeValue();
            
            NodeList startMList = routeElement.getElementsByTagName("startmileage");
            startMileage = startMList.item(0).getChildNodes().item(0)
                    .getNodeValue();
            
            NodeList endMList = routeElement.getElementsByTagName("endmileage");
            endMileage = endMList.item(0).getChildNodes().item(0)
                    .getNodeValue();
            
            NodeList occList = routeElement.getElementsByTagName("occupancy");
            occupancy = occList.item(0).getChildNodes().item(0)
                    .getNodeValue();
            
            NodeList commentList = routeElement.getElementsByTagName("comment");
            comment = commentList.item(0).getChildNodes().item(0)
                    .getNodeValue();
            
            NodeList userIDList = routeElement.getElementsByTagName("userid");
            userID = userIDList.item(0).getChildNodes().item(0)
                    .getNodeValue();
            
            //String startTime, String endTime, double startMileage, double endMileage, String comments, int totalOcuppancy, int shuttleID, int routeID
            shift = new Shift(startTime, endTime, Double.parseDouble(startMileage), 
                    Double.parseDouble(endMileage), comment, Integer.parseInt(occupancy), Integer.parseInt(shuttleID), Integer.parseInt(routeID),Integer.parseInt(userID));
            

        }

        return shift;
    }
}
