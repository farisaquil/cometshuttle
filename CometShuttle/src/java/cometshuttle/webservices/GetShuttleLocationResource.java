/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cometshuttle.webservices;

import cometshuttle.helper.Shift;
import cometshuttle.helper.Shuttle;
import cometshuttle.helper.ShuttleTracker;
import cometshuttle.helper.XMLHelper;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * REST Web Service
 *
 * @author Molten_DM
 */
@Path("getShuttleLocation")
public class GetShuttleLocationResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GetShuttleLocationResource
     */
    public GetShuttleLocationResource() {
    }

    /**
     * Retrieves representation of an instance of
     * cometshuttle.webservices.GetShuttleLocationResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/xml")
    public String getXml() {
        String content = "<location><routeid>3902901</routeid></location>";
        String response;
        try {
            int id = getLocationFromXML(content);
            Shuttle shuttle = new ShuttleTracker().getShuttleFromRouteID(id);
            response = "<location status='OK'><lat>" + shuttle.getLatitude() + "</lat>"
                    + "<lng>" + shuttle.getLongitude() + "</lng><occupancy>" + shuttle.getOccupancy() + "</occupancy>"
                    + "<capacity>" + shuttle.getCapacity() + "</capacity><status>"+shuttle.getStatus()+"</status></location>";
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(GetShuttleLocationResource.class.getName()).log(Level.SEVERE, null, ex);
            response = "<location status = 'failed'></location>";
        }
        catch(Exception e){
            Logger.getLogger(GetShuttleLocationResource.class.getName()).log(Level.SEVERE, null, e);
            response = "<location status = 'failed'></location>";
        }
        System.out.println(response);
        return response;
    }

    /**
     * PUT method for updating or creating an instance of
     * GetShuttleLocationResource
     *
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/xml")
    @Produces("application/xml")
    public String putXml(String content) {
        String response;
        try {
            int id = getLocationFromXML(content);
            Shuttle shuttle = new ShuttleTracker().getShuttleFromRouteID(id);
            response = "<location status='OK'><shuttleid>"+shuttle.getShuttleID()+"</shuttleid><lat>" + shuttle.getLatitude() + "</lat>"
                    + "<lng>" + shuttle.getLongitude() + "</lng><occupancy>" + shuttle.getOccupancy() + "</occupancy>"
                    + "<capacity>" + shuttle.getCapacity() + "</capacity><status>"+shuttle.getStatus()+"</status></location>";
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(GetShuttleLocationResource.class.getName()).log(Level.SEVERE, null, ex);
            response = "<location status = 'failed'></location>";
        }
        catch(Exception e){
            Logger.getLogger(GetShuttleLocationResource.class.getName()).log(Level.SEVERE, null, e);
            response = "<location status = 'failed'></location>";
        }
        System.out.println(response);
        return response;

    }

    private int getLocationFromXML(String xml) throws ParserConfigurationException, SAXException, IOException {

        XMLHelper xmlHelper = new XMLHelper();
        Document rootDoc = xmlHelper.loadXML(xml);

        String routeid = "";

        NodeList locationList = rootDoc.getElementsByTagName("location");

        if (locationList != null) {

            Node routeNode = locationList.item(0);
            Element routeElement = (Element) routeNode;

            NodeList idList = routeElement.getElementsByTagName("routeid");
            routeid = idList.item(0).getChildNodes().item(0)
                    .getNodeValue();

        }

        int id;

        try {
            id = Integer.parseInt(routeid);
        } catch (Exception e) {
            id = 0;
        }

        return id;
    }

}
