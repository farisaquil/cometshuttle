/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cometshuttle.webservices;

import cometshuttle.helper.Shift;
import cometshuttle.helper.User;
import cometshuttle.helper.XMLHelper;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * REST Web Service
 *
 * @author Molten_DM
 */
@Path("authentication")
public class AuthenticationResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of AuthenticationResource
     */
    public AuthenticationResource() {
    }

    /**
     * Retrieves representation of an instance of
     * cometshuttle.webservices.AuthenticationResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/xml")
    public String getXml() {
        //TODO return proper representation object
        String content = "<login><email>ajay@gmail</email><password>ajay</password></login>";
        String response;
        XMLHelper xml = new XMLHelper();
        User user = new User();
        try {

            String[] auth = getLocationFromXML(content);
            String email = auth[0];
            String pass = auth[1];

            response = user.authenticate(email, pass);
            if (response == null) {
                response = "<login status='failed'><status>";
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            response = xml.getXML(null, "failed", "");
            System.out.println(ex.getMessage());

        } catch (Exception e) {
            Logger.getLogger(SetShuttleGPSResource.class.getName()).log(Level.SEVERE, null, e);
            response = xml.getXML(null, "failed", "");
        }
        return response;
    }

    /**
     * PUT method for updating or creating an instance of AuthenticationResource
     *
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/xml")
    public String putXml(String content) {

        String response;
        XMLHelper xml = new XMLHelper();
        User user = new User();
        try {
            System.out.println(content);

            String[] auth = getLocationFromXML(content);
            String email = auth[0];
            String pass = auth[1];

            response = user.authenticate(email, pass);
            if (response == null) {
                response = "<login status='failed'></login>";
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            response = xml.getXML(null, "failed", "");
            Logger.getLogger(SetShuttleGPSResource.class.getName()).log(Level.SEVERE, null, ex);

        } catch (Exception e) {
            Logger.getLogger(SetShuttleGPSResource.class.getName()).log(Level.SEVERE, null, e);
            response = xml.getXML(null, "failed", "");
        }

        return response;
    }

    private String[] getLocationFromXML(String xml) throws ParserConfigurationException, SAXException, IOException, Exception {

        XMLHelper xmlHelper = new XMLHelper();
        Document rootDoc = xmlHelper.loadXML(xml);
        String email;
        String password;

        String[] auth = new String[2];

        //get list of all routes
        NodeList locationList = rootDoc.getElementsByTagName("login");

        if (locationList != null) {

            Node routeNode = locationList.item(0);
            Element routeElement = (Element) routeNode;

            NodeList idList = routeElement.getElementsByTagName("email");
            email = idList.item(0).getChildNodes().item(0)
                    .getNodeValue();

            NodeList routeidList = routeElement.getElementsByTagName("password");
            password = routeidList.item(0).getChildNodes().item(0)
                    .getNodeValue();

            auth[0] = email;
            auth[1] = password;
        }

        return auth;
    }
}
