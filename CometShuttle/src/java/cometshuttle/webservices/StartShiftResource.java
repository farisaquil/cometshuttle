/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cometshuttle.webservices;

import cometshuttle.helper.Route;
import cometshuttle.helper.Shuttle;
import cometshuttle.helper.ShuttleTracker;
import cometshuttle.helper.XMLHelper;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * REST Web Service
 *
 * @author Molten_DM
 */
@Path("startShift")
public class StartShiftResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of StartShiftResource
     */
    public StartShiftResource() {
    }

    /**
     * Retrieves representation of an instance of
     * cometshuttle.webservices.StartShiftResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/xml")
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of StartShiftResource
     *
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/xml")
    @Produces("application/json")
    public String putXml(String content) {
        String xml = "<shift><status>start</status><shuttleid>1</shuttleid><routeid>2323</routeid></shift>";
        int[] IDs;
        try {
            IDs = getIDsFromXML(content);
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(StartShiftResource.class.getName()).log(Level.SEVERE, null, ex);
            //IDs = null;
            return "failed";
        }
        if (IDs != null) {
            Route route = new Route(IDs[0]);
            Shuttle shuttle = new Shuttle(IDs[1]);
            if (IDs[2] == 0) {
                shuttle.setStatus(ShuttleTracker.STATUS.active);
                ShuttleTracker tracker = new ShuttleTracker();
                tracker.setActiveShuttle(route.getRouteID(), shuttle.getId());
            } else {
                shuttle.setStatus(ShuttleTracker.STATUS.inactive);
                shuttle.resetOccupancy();
                ShuttleTracker tracker = new ShuttleTracker();
                tracker.deleteActiveShuttle(route.getRouteID(), shuttle.getId());
            }
        }

        return "Success";
    }

    private int[] getIDsFromXML(String xml) throws ParserConfigurationException, SAXException, IOException {

        XMLHelper xmlHelper = new XMLHelper();
        Document rootDoc = xmlHelper.loadXML(xml);
        String shuttleID = "0";
        String routeID = "0";
        String status = "";

        //get list of all routes
        NodeList locationList = rootDoc.getElementsByTagName("shift");

        if (locationList != null) {

            Node routeNode = locationList.item(0);
            Element routeElement = (Element) routeNode;
            NodeList statusList = routeElement.getElementsByTagName("status");

            status = statusList.item(0).getChildNodes().item(0)
                    .getNodeValue();

            NodeList idList = routeElement.getElementsByTagName("routeid");

            routeID = idList.item(0).getChildNodes().item(0)
                    .getNodeValue();

            NodeList shuttleList = routeElement.getElementsByTagName("shuttleid");
            shuttleID = shuttleList.item(0).getChildNodes().item(0)
                    .getNodeValue();
 
        }
        int[] ids= new int[4];
        if (status.equalsIgnoreCase("start")) {
//            ids = {Integer.parseInt(routeID), Integer.parseInt(shuttleID)};
            ids[0] = Integer.parseInt(routeID);
            ids[1] = Integer.parseInt(shuttleID);
            ids[2] = 0;

        } else {
            ids[0] = Integer.parseInt(routeID);
            ids[1] = Integer.parseInt(shuttleID);
            ids[2] = 1;
        }

        return ids;
    }
}
