/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cometshuttle.webservices;

import cometshuttle.helper.Stats;
import cometshuttle.helper.XMLHelper;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * REST Web Service
 *
 * @author Molten_DM
 */
@Path("getStats")
public class GetStatsResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GetStatsResource
     */
    public GetStatsResource() {
    }

    /**
     * Retrieves representation of an instance of
     * cometshuttle.webservices.GetStatsResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/xml")
    public String getXml() {
        //TODO return proper representation object
        String xml = "<data><route>3902901</route><empID>100</empID><shuttleID>2</shuttleID><startDate>01/02/2015</startDate><endDate>01/07/2015</endDate></data>";
        String response;
        try {
            Stats stat = getIDsFromXML(xml);
            response = stat.getStats();

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(GetStatsResource.class.getName()).log(Level.SEVERE, null, ex);
            response = "error";
        }
        System.out.println(response);
        return response;
        
    }

    /**
     * PUT method for updating or creating an instance of GetStatsResource
     *
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/xml")
    @Produces("application/xml")
    public String putXml(String content) {
        String xml = "<data><route></route><empID></empID><shuttleID></shuttleID><startDate></startDate><endDate></endDate></data>";
        String response;
        try {
            Stats stat = getIDsFromXML(xml);
            response = stat.getStats();

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(GetStatsResource.class.getName()).log(Level.SEVERE, null, ex);
            response = "error";
        }
        return response;

    }

    private Stats getIDsFromXML(String xml) throws ParserConfigurationException, SAXException, IOException {

        XMLHelper xmlHelper = new XMLHelper();
        Document rootDoc = xmlHelper.loadXML(xml);
        String shuttleID = "0";
        String route = "0";
        String id = "";
        String start = "";
        String end = "";

        //get list of all routes
        NodeList locationList = rootDoc.getElementsByTagName("data");

        if (locationList != null) {

            Node routeNode = locationList.item(0);
            Element routeElement = (Element) routeNode;

            NodeList routeList = routeElement.getElementsByTagName("route");
            route = routeList.item(0).getChildNodes().item(0)
                    .getNodeValue();

            NodeList idList = routeElement.getElementsByTagName("empID");
            id = idList.item(0).getChildNodes().item(0)
                    .getNodeValue();

            NodeList shuttleList = routeElement.getElementsByTagName("shuttleID");
            shuttleID = shuttleList.item(0).getChildNodes().item(0)
                    .getNodeValue();

            NodeList startList = routeElement.getElementsByTagName("startDate");
            start = startList.item(0).getChildNodes().item(0)
                    .getNodeValue();

            NodeList endList = routeElement.getElementsByTagName("endDate");
            end = endList.item(0).getChildNodes().item(0)
                    .getNodeValue();

        }
        //int routeID, int userID, int shuttleID, String startDate, String endDate
        Stats stat = new Stats(Integer.parseInt(route), Integer.parseInt(id), Integer.parseInt(shuttleID), start, end);

        return stat;
    }
}
