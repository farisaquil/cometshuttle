/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cometshuttle.webservices;

import cometshuttle.helper.ShuttleRoutes;
import cometshuttle.helper.User;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;


/**
 * REST Web Service
 *
 * @author Molten_DM
 */
@Path("getroutes")
public class GetRoutes {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GetRoutes
     */
    public GetRoutes() {
    }

    /**
     * Retrieves representation of an instance of cometshuttle.helper.GetRoutes
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/xml")
    public String getXml() {
        //(String username, String password, String fullName, String email, String phone, int type)
//        User user = new User();
////        user.addUser("ajay@gmail", "ajay","ajay","575658", 200);
//        user.addUser("divya@gmail", "divya","Divya","36236", 200);
//        user.addUser("jack@gmail", "jack","Jack","2342", 200);
//        user.addUser("bob@gmail", "bob","Bob","52452", 200);
        //TODO return proper representation object
        ShuttleRoutes routes = new ShuttleRoutes();
        routes.getRoutesFromDB();
        String xml = routes.toXMLString();
        System.out.println(xml);

        return xml;

    }

    /**
     * PUT method for updating or creating an instance of GetRoutes
     * @param request representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/xml")
    public String putXml(String request) {
//        ShuttleRoutes routes = new ShuttleRoutes();
//        routes.getRoutesFromDB();
//        String xml = routes.toString();
        
         
        return null;//response;
    }
}
