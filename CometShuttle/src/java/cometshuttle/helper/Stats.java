/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cometshuttle.helper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Molten_DM
 */
public class Stats {

    String xml = "<data><route></route><empID></empID><shuttleID></shuttleID><startDate></startDate><endDate></endDate></data>";

    int routeID;
    int userID;
    int shuttleID;
    String startDate;
    String endDate;

    public Stats(int routeID, int userID, int shuttleID, String startDate, String endDate) {
        this.routeID = routeID;
        this.userID = userID;
        this.shuttleID = shuttleID;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getXml() {
        return xml;
    }

    public String getStats() {
        String query = "SELECT sh.[user_id], l.fullname, sh.starttime, sh.endtime, sh.startmil, sh.endmil, sh.tot_occup, s.shuttle_id, r.route_id "
                + "from Shift sh "
                + "join Route r "
                + "on r.route_id = sh.route_id "
                + "join Shuttles s "
                + "on s.shuttle_id = sh.shuttle_id "
                + "join [Login] l "
                + "on l.[user_id] = sh.[user_id] "
                + "where sh.starttime >= ? and sh.endtime <= ? "
                + "AND sh.shuttle_id = ? "
                + "AND sh.route_id = ? "
                + "AND l.user_id = ? ";
        SQLConnect sql = new SQLConnect();
        String result = "<data>";
        
        try {
            ResultSet rs = sql.executeSelectQuery(query, startDate, endDate, shuttleID, routeID, userID);
            String fullName;
            String startTime;
            String endTime;
            double startMileage;
            double endMileage;
            int occupancy;
//            int shuttleID;
//            int routeID;

            while (rs.next()) {
                String row;
                fullName = rs.getString("fullname");
                startTime = rs.getString("starttime");
                endTime = rs.getString("endtime");
                startMileage = rs.getDouble("startmil");
                endMileage = rs.getDouble("endmil");
                occupancy = rs.getInt("tot_occup");
                shuttleID = rs.getInt("shuttle_id");
                routeID = rs.getInt("route_id");
                
                row = "<row><fullname>" + fullName + "</fullname><starttime>" + startTime + "</starttime><endtime>" + endTime + "</endtime>"
                        + "<startmileage>" + startMileage + "/startmileage><endmileage>" + endMileage + "</endmileage>"
                        + "<totaloccupancy>" + occupancy + "/totaloccupancy><shuttleid>" +shuttleID +  "</shuttleid>"
                        +"<routeid>" + routeID + "</routeid></row>";
                result += row;

                //routeID = rs.getInt("route_id");           
            }
            result+="</data>";
            sql.closeConnection();
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ShuttleTracker.class.getName()).log(Level.SEVERE, null, ex);
            return "error";
        }

        return result;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public int getRouteID() {
        return routeID;
    }

    public void setRouteID(int routeID) {
        this.routeID = routeID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getShuttleID() {
        return shuttleID;
    }

    public void setShuttleID(int shuttleID) {
        this.shuttleID = shuttleID;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

}
