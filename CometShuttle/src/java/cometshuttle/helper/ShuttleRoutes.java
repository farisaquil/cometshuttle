package cometshuttle.helper;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Molten_DM on 3/25/2015.
 */
public class ShuttleRoutes {

    ArrayList<Route> routes;

    public ShuttleRoutes() {

    }

    public ArrayList<Route> getRoutes() {

        return routes;
    }

    public ArrayList<Route> getRoutesFromDB() {
        routes = new ArrayList<>();
        SQLConnect sql = new SQLConnect();
        String query = "SELECT * FROM Route";
        try {
            ResultSet rs = sql.executeSelectQuery(query);
            while (rs.next()) {

                int id = rs.getInt("route_id");
                String name = rs.getString("route_name");
                String color = rs.getString("color");
                Route route = new Route(id, name, color);
                route.setPointsFromDB();
                Collections.sort(route.getPoints());

                routes.add(route);
            }
            sql.closeConnection();

        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Route.class.getName()).log(Level.SEVERE, null, ex);
            sql.closeConnection();
        }
        return routes;
    }

    public void createRoutesFromXML(String xml) throws ParserConfigurationException, SAXException, IOException {
        XMLHelper xmlHelper = new XMLHelper();
        Document rootDoc = xmlHelper.loadXML(xml);
        routes = new ArrayList<>();

        //get list of all routes
        NodeList routeslist = rootDoc.getElementsByTagName("route");

        if (routeslist != null && routeslist.getLength() > 0) {
            for (int route = 0; route < routeslist.getLength(); route++) {
                Node routeNode = routeslist.item(route);
                Element routeElement = (Element) routeNode;
                String color = routeElement.getAttribute("color");

                NodeList namesList = routeElement.getElementsByTagName("name");

                String name = namesList.item(0).getChildNodes().item(0)
                        .getNodeValue();

                NodeList pointList = routeElement.getElementsByTagName("point");
                if (pointList != null && pointList.getLength() > 0) {
                    Route newRoute = new Route();
                    newRoute.setName(name);
                    for (int point = 0; point < pointList.getLength(); point++) {

                        Node latNode = pointList.item(point);
                        Element geoElement = (Element) latNode;
                        NodeList lat = geoElement.getElementsByTagName("lat");
                        double originLat = Double.parseDouble(lat.item(0).getChildNodes().item(0)
                                .getNodeValue());

                        NodeList lng = geoElement.getElementsByTagName("lng");
                        double originLng = Double.parseDouble(lng.item(0).getChildNodes().item(0)
                                .getNodeValue());
                        LatLng2 pointA = new LatLng2(originLat, originLng);

                        RoutePoint routePoint = new RoutePoint(pointA);
                        routePoint.setSortOrder();
                        newRoute.addPoint(routePoint, color);

                    }
                    routes.add(newRoute);
                }
            }

        }

    }

    public void storeRoutesInDB() {
        routes.stream().forEach((route) -> {
            route.storeRouteInDB();
        });
    }

    public String toXMLString() {
        String xmlString = "<routes>";
        for (Route route : routes) {
            xmlString += route.toXMLString();
        }
        xmlString += "</routes>";
        return xmlString;
    }
}
