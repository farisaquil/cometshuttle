package cometshuttle.helper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Molten_DM on 3/25/2015.
 */
public class RoutePoint implements Comparable<RoutePoint> {

    private final LatLng2 pointA;
    //private final LatLng pointB;
    private final SQLConnect sql;
    int order;
    static int SORT_ORDER = 0;
//    private final XMLHelper xml;
    private int pointID;

    public RoutePoint(LatLng2 pointA) {
        this.pointA = pointA;//new LatLng(32.987539, -96.747038);
        //this.pointB = pointB;//new LatLng(32.987638, -96.747027);
        sql = new SQLConnect();
//        xml = new XMLHelper();
    }

    public void setSortOrder() {
        order = SORT_ORDER++;
    }

    public void resetSortOrder() {
        SORT_ORDER = 0;
    }

    public int getPointID() {
        return pointID;
    }

    public LatLng2 getPointA() {
        return pointA;
    }

    public void setPointId(int pointID) {
        this.pointID = pointID;
    }

    public void setOrder(int order) {
        this.order = order;
    }

//    public LatLng getPointB() {
//        return pointB;
//    }
    public boolean storePointInDB(int routeID) {
        while (true) {
            int number = generateID();
            if (checkAvailablity(number)) {
                pointID = number;
                break;
            }
        }
        try {
            String query = "INSERT INTO Points VALUES (?,?,?,?,?)";
            boolean flag = sql.executeUpdateQuery(query, pointID, routeID, order, pointA.latitude, pointA.longitude);//), pointB.latitude, pointB.longitude);
            this.sql.closeConnection();/////////////////
            return flag;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Route.class.getName()).log(Level.SEVERE, null, ex);
            this.sql.closeConnection();
            return false;
        }
    }

    public boolean checkAvailablity(int number) {
        int numberTemp = -1;
        String query = "SELECT point_id FROM Points WHERE point_id = ?";
        try {
            ResultSet rs = sql.executeSelectQuery(query, number);
            while (rs.next()) {
                numberTemp = rs.getInt("route_id");
            }
            this.sql.closeConnection();

        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Route.class.getName()).log(Level.SEVERE, null, ex);
            this.sql.closeConnection();
        }
        
        return numberTemp == -1;
    }

    public int generateID() {
        Random random = new Random();
        int number = random.nextInt(10000000);
        return number;
    }

    public String toXMLString() {
        return "<point><lat>" + pointA.latitude + "</lat><lng>" + pointA.longitude + "</lng></point>";
    }

    @Override
    public int compareTo(RoutePoint o) {
        if (this.order < o.order) {
            return -1;
        } else if (this.order > o.order) {
            return 1;
        } else {
            return 0;
        }
    }

}
