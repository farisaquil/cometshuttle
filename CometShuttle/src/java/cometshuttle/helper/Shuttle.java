/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cometshuttle.helper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Molten_DM
 */
public class Shuttle {

    int id;
    double latitude;
    double longitude;
    int capacity;
    volatile int occupancy;
    double mileage;
    int routeID;
    ShuttleTracker.STATUS status;
    SQLConnect sql;
    XMLHelper xml;

    public Shuttle() {

    }

    public Shuttle(int id) {
        sql = new SQLConnect();
        String query = "SELECT * FROM Shuttles WHERE shuttle_id = ?";
        try {
            ResultSet rs = sql.executeSelectQuery(query, id);
            while (rs.next()) {

                this.id = id;
                latitude = rs.getDouble("latitude");
                longitude = rs.getDouble("longitude");
                mileage = rs.getDouble("mileage");
                capacity = rs.getInt("capacity");
                occupancy = rs.getInt("occupancy");
                status = ShuttleTracker.STATUS.valueOf(rs.getString("status"));
                //routeID = rs.getInt("route_id");           
            }
            sql.closeConnection();
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ShuttleTracker.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    
    public int getOccupancy(){
        return this.occupancy;
    }

    public Shuttle(int id, double latitude, double longitude, int capacity, double mileage, ShuttleTracker.STATUS status) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.capacity = capacity;
        this.mileage = mileage;
        this.status = status;
    }

    public String getStatus() {
        return status.toString();
    }
    public String requestShuttle(int routeID, String time, double lat, double lng){
        String query = "INSERT INTO Heatmap values(?,?,?,?)";
        sql = new SQLConnect();
        xml = new XMLHelper();
        try {

            boolean flag = sql.executeUpdateQuery(query, id, routeID, time, lat, lng);

            if (flag) {

                return xml.getXML(null, "OK", "");

            } else {
                return xml.getXML(null, "failed", "");
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Route.class.getName()).log(Level.SEVERE, null, ex);
            return xml.getXML(null, "failed", "");
        }
        
        
    }

    /**
     * 0 = add 1 = subtract
     *
     * @param status
     */
    public void updateOccupancy(int status) {
        sql = new SQLConnect();
        try {
            if (status == 0) {
                if (occupancy < capacity) {
                    occupancy += 1;
                    String query = "UPDATE Shuttles SET occupancy = ? WHERE shuttle_id = ?";

                    sql.executeUpdateQuery(query, occupancy, id);
                }

            } else {
                if (occupancy != 0) {
                    occupancy -= 1;
                    String query = "UPDATE Shuttles SET occupancy = ? WHERE shuttle_id = ?";
                    sql.executeUpdateQuery(query, occupancy, id);
                }

            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ShuttleTracker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void resetOccupancy() {
        sql = new SQLConnect();
        try {

            String query = "UPDATE Shuttles SET occupancy = 0 WHERE shuttle_id = ?";

            sql.executeUpdateQuery(query, occupancy, id);

        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ShuttleTracker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setStatus(ShuttleTracker.STATUS status) {
        this.status = status;
        sql = new SQLConnect();
        String query = "UPDATE Shuttles SET status = ? WHERE shuttle_id = ?";
        try {
            sql.executeUpdateQuery(query, status.toString(), id);

        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ShuttleTracker.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String setRouteID(int routeID) {
        this.routeID = routeID;
        String query = "INSERT INTO ActiveShuttles VALUES (?,?)";
        sql = new SQLConnect();
        xml = new XMLHelper();
        try {

            boolean flag = sql.executeUpdateQuery(query, id, routeID);

            if (flag) {

                return xml.getXML(null, "OK", "");

            } else {
                return xml.getXML(null, "failed", "");
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Route.class.getName()).log(Level.SEVERE, null, ex);
            return xml.getXML(null, "failed", "");
        }

    }

    public void setLocation2DB() {
        String query = "UPDATE Shuttles set latitude = ?, longitude = ? WHERE shuttle_id = ?";
        sql = new SQLConnect();
        try {
            sql.executeUpdateQuery(query, latitude, longitude, id);

        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ShuttleTracker.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public int getShuttleID(){
        return this.id;
    }
    public double[] getLocationFromDB() {
        String query = "SELECT latitude, longitude FROM Shuttles WHERE shuttle_id = ?";
        double[] location = new double[2];
        try {
            ResultSet rs = sql.executeSelectQuery(query, id);
            while (rs.next()) {

                latitude = rs.getDouble("latitude");
                longitude = rs.getDouble("longitude");
                location[0] = latitude;
                location[1] = longitude;

            }
            sql.closeConnection();
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ShuttleTracker.class.getName()).log(Level.SEVERE, null, ex);
        }

        return location;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getMileage() {
        return mileage;
    }

    public void setMileage(double mileage) {
        this.mileage = mileage;

    }

    public String toXMLString() {

        String xmlString = "<shuttle><id>" + id + "</id>" + "<capacity>" + capacity + "</capacity>" + "<mileage>" + mileage + "</mileage>"
                + "<status>" + status.toString() + "</status><routeid>" + routeID + "</routeid>";

        xmlString += "</shuttle>";
        return xmlString;
    }

}
