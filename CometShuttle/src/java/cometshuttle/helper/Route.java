package cometshuttle.helper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Molten_DM on 3/25/2015.
 */
public class Route {

    private ArrayList<RoutePoint> points;
    private int routeID;
    private String color;
    private String name;
    SQLConnect sql;
    XMLHelper xml;

    public Route() {
        points = new ArrayList<>();
        sql = new SQLConnect();
        xml = new XMLHelper();
    }
    public Route(int routeID){
        sql = new SQLConnect();
        String query = "SELECT * FROM Route WHERE route_id = ?";
        try {
            ResultSet rs = sql.executeSelectQuery(query, routeID);
            while (rs.next()) {

                this.routeID = routeID;
                color = rs.getString("color");
                name = rs.getString("route_name");          
            }
            sql.closeConnection();
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ShuttleTracker.class.getName()).log(Level.SEVERE, null, ex); 
            sql.closeConnection();
        }
       
    }

    public Route(int routeID, String name, String color) {
        this.routeID = routeID;
        this.color = color;
        this.name = name;
        points = new ArrayList<>();
        sql = new SQLConnect();
    }

    public void setPointsFromDB() {
        String query = "SELECT * FROM Points WHERE route_id = ? ORDER BY sort_order";
        try {
            ResultSet rs = sql.executeSelectQuery(query, routeID);
            while (rs.next()) {
                int pointID = rs.getInt("point_id");
                int order = rs.getInt("sort_order");
                double lat = rs.getDouble("latitude");
                double lng = rs.getDouble("longitude");
                LatLng2 point = new LatLng2(lat, lng);
                
                
                RoutePoint routePoint = new RoutePoint(point);
                routePoint.setPointId(pointID);
                routePoint.setOrder(order);
                points.add(routePoint);
            }
            this.sql.closeConnection();

        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Route.class.getName()).log(Level.SEVERE, null, ex);
            this.sql.closeConnection();
        }
        

    }
    public void setPoints(ArrayList<RoutePoint> points){
        this.points = points;
    }

    public boolean checkAvailablity(int number) {
        int numberTemp = -1;
        String query = "SELECT route_id FROM Route WHERE route_id = ?";
        try {
            ResultSet rs = sql.executeSelectQuery(query, number);
            while (rs.next()) {
                numberTemp = rs.getInt("route_id");
            }
            this.sql.closeConnection();

        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Route.class.getName()).log(Level.SEVERE, null, ex);
            this.sql.closeConnection();
        }
        return numberTemp == -1;
    }

    public int generateID() {
        Random random = new Random();
        int number = random.nextInt(10000000);
        return number;
    }

    public ArrayList<RoutePoint> getPoints() {
        return points;
    }

    public void addPoint(RoutePoint point, String color) {
        points.add(point);
        this.color = color;
    }

    public int getRouteID() {
        return routeID;
    }

    public void setRouteID(int routeID) {
        this.routeID = routeID;
    }

    /**
     * Stores route information to the server
     *
     * @return an xml response
     */
    public String storeRouteInDB() {

        while (true) {
            int number = generateID();
            if (checkAvailablity(number)) {
                routeID = number;
                break;
            }
        }

        try {
            String query = "INSERT INTO Route VALUES (?,?,?)";
            boolean flag = sql.executeUpdateQuery(query, routeID, name, color);

            if (flag) {
                storePointsInDB();
                this.sql.closeConnection();
                return xml.getXML(null, "OK", "");

            } else {
                this.sql.closeConnection();
                return xml.getXML(null, "failed", "");
            }
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Route.class.getName()).log(Level.SEVERE, null, ex);
            this.sql.closeConnection();
            return xml.getXML(null, "failed", "");
        }
        

    }

    /**
     * Traverses the points ArrayList and stores each point in the database
     */
    private void storePointsInDB() throws SQLException, ClassNotFoundException {

        //used to be a for each loop
        for (RoutePoint point : points) {

            point.storePointInDB(routeID);
//            String query = "INSERT INTO RoutePoints VALUES (?,?)";
//            sql.executeUpdateQuery(query, routeID, point.getPointID());

        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String toXMLString() {

        String xmlString = "<route color='" + color + "'><id>" + routeID + "</id><name>" + name + "</name>";
        for (RoutePoint point : points) {
            String temp = point.toXMLString();
            xmlString += point.toXMLString();
        }

        xmlString += "</route>";
        return xmlString;
    }

}
