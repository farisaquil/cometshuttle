/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cometshuttle.helper;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Faris
 */
public class XMLHelper {

    public XMLHelper() {
        

    }
    
    public String getXML2(ResultSet rs, String status, String rootNode) {

        Document doc;
        String xml;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            doc = builder.newDocument();
            Element results = doc.createElement("routes");
            //results.setAttribute("status", status);
            doc.appendChild(results);

            if (rs != null) {
                ResultSetMetaData rsmd = rs.getMetaData();
                int colCount = rsmd.getColumnCount();

                while (rs.next()) {
                    Element user = doc.createElement(rootNode);
                    results.appendChild(user);
                    
                    for (int i = 1; i <= colCount; i++) {
                        String columnName = rsmd.getColumnName(i);
                        Object value = rs.getObject(i);
                        Element node = doc.createElement(columnName);
                        if (value != null) {
                            node.appendChild(doc.createTextNode(value.toString()));
                            user.appendChild(node);
                        }
                    }
                }
            }
            xml = transformXML(doc);
        } catch (SQLException | ParserConfigurationException | TransformerException e) {
            xml = null;
        }
        return xml;
    }
    
    
    
    

    /**
     * Recieves a result set and transfrom it into an xml string
     *
     * @param rootNode
     * @param rs
     * @param status
     * @return the xml string
     */
    public String getXML(ResultSet rs, String status, String rootNode) {

        Document doc;
        String xml;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            doc = builder.newDocument();
            Element results = doc.createElement("Results");
            results.setAttribute("status", status);
            doc.appendChild(results);

            if (status.equals("valid") && rs != null) {
                ResultSetMetaData rsmd = rs.getMetaData();
                int colCount = rsmd.getColumnCount();

                while (rs.next()) {
                    Element user = doc.createElement(rootNode);
                    results.appendChild(user);
                    for (int i = 1; i <= colCount; i++) {
                        String columnName = rsmd.getColumnName(i);
                        Object value = rs.getObject(i);
                        Element node = doc.createElement(columnName);
                        if (value != null) {
                            node.appendChild(doc.createTextNode(value.toString()));
                            user.appendChild(node);
                        }
                    }
                }
            }
            xml = transformXML(doc);
        } catch (SQLException | ParserConfigurationException | TransformerException e) {
            xml = null;
        }
        return xml;
    }

    public Document loadXML(String xml) throws ParserConfigurationException, SAXException, IOException {
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource insrc = new InputSource(new StringReader(xml));
        return builder.parse(insrc);
    }

    /**
     * Sets the xmls properties
     *
     * @param doc
     * @return
     * @throws TransformerConfigurationException
     * @throws TransformerException
     */
    public String transformXML(Document doc) throws TransformerConfigurationException, TransformerException {
        DOMSource domSource = new DOMSource(doc);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
        StringWriter sw = new StringWriter();
        StreamResult sr = new StreamResult(sw);
        transformer.transform(domSource, sr);
        return sw.toString();
    }
    /**
     * creates the header each XML shares based on the type of the request
     *
     * @param type
     * @return
     * @throws ParserConfigurationException
     */
    public Document createXMLHeader(String type)
            throws ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();
        Element request = doc.createElement("request");
        request.setAttribute("type", type);
        doc.appendChild(request);
        return doc;
    }
}
