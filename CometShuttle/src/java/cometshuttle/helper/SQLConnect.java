package cometshuttle.helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.PreparedStatement;

/**
 * This is the class used to connect to a SQL database
 *
 * @author Faris
 *
 */
public class SQLConnect {

    private static Connection connectSQL;
    private static Statement stmt;   
    private static PreparedStatement updatePrepared;
    
    private static String ip;
    private static String port;
    private static String catalog;
    private static String username;
    private static String password;

    /**
     * The constructor retrieves all the necessary information to connect from
     * the strings.xml file in the res/values folder
     */
    public SQLConnect() {
//        ip = "k2yczwyqve.database.windows.net";
//        port = "1433";
//        catalog = "GoldenDataSQL";
//        username = "goldendata";
//        password = "F@ris@quil";
        stmt = null;
        connectSQL = null;
    }



    /**
     * Connects to the a MySql Database
     *
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    @Deprecated
    public boolean connectDatabaseOld() throws ClassNotFoundException,
            SQLException {
        String urlconnectionMySQL = "jdbc:mysql://" + ip + ":" + port + "/"
                + catalog;

        //String urlconnectionMySQL = "jdbc:mysql://us-cdbr-azure-west-a.cloudapp.net:3306/GoldenDataMySQL";
        boolean flag;

        Class.forName("com.mysql.jdbc.Driver");

        connectSQL = DriverManager.getConnection(urlconnectionMySQL,
                username, password);

        flag = true;
        return flag;

    }

    /**
     * Connects to the a SQL Database
     *
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public boolean connectDatabase() throws ClassNotFoundException,
            SQLException {
        String JDBCDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        // String urlconnectionMySQL =
        // "jdbc:mysql://b177631ea47081:cc118ea3@us-cdbr-azure-west-a.cloudapp.net/as_4b5e6ee9c0176f7";
//       String urlconnectionMySQL = "jdbc:sqlserver://qr8dxjckxb.database.windows.net:1433;database=CometShuttleDB;"
//                + "user=shuttle@qr8dxjckxb;password=Comet123;"
//                + "encrypt=true;hostNameInCertificate=*.database.windows.net;loginTimeout=30;";
        String urlconnectionMySQL = "jdbc:sqlserver://qr8dxjckxb.database.windows.net:1433;database=CometShuttleDB;user=shuttle@qr8dxjckxb;password=Comet123;encrypt=true;hostNameInCertificate=*.database.windows.net;loginTimeout=30;";
        boolean flag;

        Class.forName(JDBCDriver);

        //;user=goldendata;password=F@ris@quil;databaseName=goldendatadb
        connectSQL = DriverManager.getConnection(urlconnectionMySQL);

        flag = true;
        return flag;

    }

    /**
     * Closes the connection
     */
    public void closeConnection() {
        try {
            if (stmt != null) {
                stmt.close();
            }
            if (connectSQL != null) {
                connectSQL.close();
            }
        } catch (SQLException e) {
            e.getMessage();
        }

    } 

    /**
     * Executes a prepred "SELECT" statement DON NOT EXECUTE UPDATE STATEMENTS
     *
     * @param <T>
     * @param query to be set in a prepared statement
     * @param keywords
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public <T> ResultSet executeSelectQuery(String query, T... keywords)
            throws SQLException, ClassNotFoundException {

        boolean flag = connectDatabase();
        ResultSet rs;
        if (flag == false) {
            return null;
        }
        updatePrepared = connectSQL.prepareStatement(query);

        for (int i = 1; i <= keywords.length; i++) {

            if (keywords[i - 1] instanceof String) {
                updatePrepared.setString(i, (String) keywords[i - 1]);
            } else if (keywords[i - 1] instanceof Integer) {
                updatePrepared.setInt(i, (Integer) keywords[i - 1]);
            } else if (keywords[i - 1] instanceof Boolean) {
                updatePrepared.setBoolean(i, (Boolean) keywords[i - 1]);
            } else if (keywords[i - 1] instanceof byte[]) {
                updatePrepared.setBytes(i, (byte[]) keywords[i - 1]);
            } else if (keywords[i - 1] instanceof Double) {
                updatePrepared.setDouble(i, (Double) keywords[i - 1]);
            } else {
                return null;
            }
        }

        rs = updatePrepared.executeQuery();

        return rs;
    }

    /**
     * Executes a prepred "UPDATE" statement DO NOT EXECUTE SELECT STATEMENTS
     * parameters are generic, only accepts String, int, double, boolean, or
     * byte[]
     *
     * @param <T>
     * @param keywords the values to be inserted in the query in the right order
     * @param query to be set in a prepared statement
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public <T> boolean executeUpdateQuery(String query, T... keywords)
            throws SQLException, ClassNotFoundException {

        boolean flag = connectDatabase();
        if (flag == false) {
            return false;
        }
        updatePrepared = connectSQL.prepareStatement(query);
        for (int i = 1; i <= keywords.length; i++) {

            if (keywords[i - 1] instanceof String) {
                updatePrepared.setString(i, (String) keywords[i - 1]);
            } else if (keywords[i - 1] instanceof Integer) {
                updatePrepared.setInt(i, (Integer) keywords[i - 1]);
            } else if (keywords[i - 1] instanceof Boolean) {
                updatePrepared.setBoolean(i, (Boolean) keywords[i - 1]);
            } else if (keywords[i - 1] instanceof byte[]) {
                updatePrepared.setBytes(i, (byte[]) keywords[i - 1]);
            } else if (keywords[i - 1] instanceof Double) {
                updatePrepared.setDouble(i, (Double) keywords[i - 1]);
            } else {

                return false;
            }

        }
        updatePrepared.executeUpdate();
        closeConnection();
        return true;
    }

    /**
     * The search engine on the temp tables created not the final version
     *
     * @param keywords
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public ArrayList<String> searchDB(String[] keywords)
            throws ClassNotFoundException, SQLException {

        ArrayList<String> response = new ArrayList<>();
        ResultSet rs;
        String query = "SELECT DISTINCT a.restid FROM restaurant a JOIN rest_type b ON b.restid = a.restid WHERE "
                + "(CONCAT(a.rest_name, b.rest_type) LIKE ?)";
        for (int index = 1; index < keywords.length; index++) {
            query += " AND (CONCAT(a.rest_name, b.rest_type) LIKE ?)";
        }
        for (int i = 1; i <= keywords.length; i++) {

            keywords[i - 1] = "%" + keywords[i - 1] + "%";
        }
        rs = executeSelectQuery(query, keywords);
        while (rs.next()) {
            response.add(rs.getString("restid"));
        }
        closeConnection();
        return response;
    }

}
