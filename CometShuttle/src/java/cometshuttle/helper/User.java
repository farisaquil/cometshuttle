package cometshuttle.helper;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

public class User {

    private final XMLHelper xml;
    private final SQLConnect sql;

    public User() {
        xml = new XMLHelper();
        sql = new SQLConnect();
    }

    public String authenticate(String email, String password) {

        try {
            String query = "SELECT passwd,salt FROM Login WHERE email = ?";
            byte[] encryptedPassword = null;
            byte[] salt = null;
            PasswordEncryptionService encrypt = new PasswordEncryptionService();

            ResultSet rs = sql.executeSelectQuery(query,
                    email);
            while (rs.next()) {
                encryptedPassword = rs.getBytes("passwd");
                salt = rs.getBytes("salt");
            }
            boolean flag;

            if (encryptedPassword != null && salt != null) {

                flag = encrypt.authenticate(password, encryptedPassword, salt);
            } else {

                flag = false;
            }
            sql.closeConnection();
            if (flag == true) {
                return getUserXML(email);
            } else {
                return null;
            }
        } catch (SQLException | ClassNotFoundException | NoSuchAlgorithmException | InvalidKeySpecException ex) {
            return null;
        }
    }

    public String addUser(String email, String password, String fullName, String phone, int type) {

        try {
            PasswordEncryptionService passEncrypt = new PasswordEncryptionService();
            byte[] salt;
            byte[] encryptedPassword;
            salt = passEncrypt.generateSalt();
            encryptedPassword = passEncrypt.getEncryptedPassword(password, salt);

//            String query = "INSERT INTO Login(username,passwd,salt,type_id) VALUES(?,?,?,?)";
            String query = "INSERT INTO Login(email,passwd,salt,fullname, phone, type_id) VALUES(?,?,?,?,?,?)";

            if (encryptedPassword != null && salt != null) {

                sql.executeUpdateQuery(query, email,
                        encryptedPassword, salt, fullName, phone, type);

                this.sql.closeConnection();
                return getUserXML(email);

            } else {
                return null;
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | SQLException | ClassNotFoundException ex) {
            return null;
        }
    }

    public String getUserXML(String email) throws SQLException, ClassNotFoundException {

        String query = "SELECT user_id FROM Login WHERE email = ?";

        ResultSet rs = sql.executeSelectQuery(query, email);
        int id = 0;
        while (rs.next()) {

            id = rs.getInt("user_id");

        }
        sql.closeConnection();

        String xmlResponse = "<login status='OK'><email>"+email+"</email><id>"+id+"</id></login>";
        this.sql.closeConnection();
        return xmlResponse;
    }

    public String checkEmail(String email) throws ClassNotFoundException,
            SQLException,
            ParserConfigurationException,
            TransformerException {

        String query = "SELECT email FROM Login WHERE email = ?";
        String tempEmail = null;
        ResultSet rs = sql.executeSelectQuery(query, email);
        while (rs.next()) {
            tempEmail = rs.getString("email");
        }
        if (tempEmail == null) {
            sql.closeConnection();
            return xml.getXML(null, "available", "");
        } else {
            sql.closeConnection();
            return xml.getXML(null, "taken", "");
        }
    }

    public String addBusiness2User(String businessName, String website, boolean delivery, int userID)
            throws SQLException, ClassNotFoundException, ParserConfigurationException, TransformerException {

        String query = "INSERT INTO Business(business_name, website, delivery, user_id) values(?,?,?,?)";
        boolean flag = sql.executeUpdateQuery(query, businessName, website, delivery, userID);

        this.sql.closeConnection();
        if (flag) {
            return xml.getXML(null, "OK", "");
        } else {
            return xml.getXML(null, "failed", "");
        }

    }

}
