package cometshuttle.helper;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Molten_DM on 4/24/2015.
 */
public class Shift implements Serializable {

    String startTime;
    String endTime;
    double startMileage;
    double endMileage;
    String comments;
    int totalOcuppancy;
    int shuttleID;
    int userID;
    int routeID;
    SQLConnect sql;
    XMLHelper xml;

    public Shift(String startTime, String endTime, double startMileage, double endMileage, String comments, int totalOcuppancy, int shuttleID, int routeID, int userID) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.startMileage = startMileage;
        this.endMileage = endMileage;
        this.comments = comments;
        this.totalOcuppancy = totalOcuppancy;
        this.shuttleID = shuttleID;
        this.routeID = routeID;
        this.userID = userID;
    }

    public String add2DB() {
        String query = "insert into Shift (starttime, endtime, startmil, endmil, comments, tot_occup, route_id,shuttle_id, "
                + "[user_id]) values (?,?,?,?,?,?,?,?,?);";
        sql = new SQLConnect();
        xml = new XMLHelper();
        try {
            boolean flag = sql.executeUpdateQuery(query, startTime, endTime, startMileage, endMileage, comments, totalOcuppancy, routeID, shuttleID, userID);

            if (flag) {
                return xml.getXML(null, "OK", "");

            } else {
                return xml.getXML(null, "failed", "");
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Route.class.getName()).log(Level.SEVERE, null, ex);
            return xml.getXML(null, "failed", "");
        }

    }

    public Shift() {
        totalOcuppancy = 0;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public void updateOccupancy() {
        totalOcuppancy += 1;
    }

    public double getStartMileage() {
        return startMileage;
    }

    public void setStartMileage(double startMileage) {
        this.startMileage = startMileage;
    }

    public double getEndMileage() {
        return endMileage;
    }

    public void setEndMileage(double endMileage) {
        this.endMileage = endMileage;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public int getTotalOcuppancy() {
        return totalOcuppancy;
    }

    public void setTotalOcuppancy(int totalOcuppancy) {
        this.totalOcuppancy = totalOcuppancy;
    }

    public int getShuttleID() {
        return shuttleID;
    }

    public void setShuttleID(int shuttleID) {
        this.shuttleID = shuttleID;
    }

    public int getRouteID() {
        return routeID;
    }

    public void setRouteID(int routeID) {
        this.routeID = routeID;
    }
}
