/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cometshuttle.helper;

/**
 *
 * @author Molten_DM
 */
public class LatLng2 {
    
    double latitude;
    double longitude;

    public LatLng2(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
    

    public double getlatitude() {
        return latitude;
    }

    public void setlatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getlongitude() {
        return longitude;
    }

    public void setlongitude(double longitude) {
        this.longitude = longitude;
    }
    
    
}
