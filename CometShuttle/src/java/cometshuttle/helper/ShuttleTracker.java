/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cometshuttle.helper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Molten_DM
 */
public class ShuttleTracker {

    SQLConnect sql;
    private static ArrayList<Shuttle> shuttles;

    public ShuttleTracker() {
        sql = new SQLConnect();
        shuttles = new ArrayList<>();
    }

    public ArrayList<Shuttle> getShuttlesFromDB() {
        try {
            String query = "SELECT * FROM Shuttles";
            ResultSet rs = sql.executeSelectQuery(query);
            while (rs.next()) {

                int shuttle_id = rs.getInt("shuttle_id");
                double latitude = rs.getDouble("latitude");
                double longitude = rs.getDouble("longitude");
                double mileage = rs.getDouble("mileage");
                int capacity = rs.getInt("capacity");
                String status = rs.getString("status");

                Shuttle shuttle = new Shuttle(shuttle_id, latitude, longitude, capacity, mileage, setStatus(status));

                shuttles.add(shuttle);
            }
            sql.closeConnection();
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ShuttleTracker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return shuttles;
    }
    
    public Shuttle getShuttleFromRouteID(int routeID){
        Shuttle shuttle = null;
        String query = "SELECT shuttle_id FROM ActiveShuttles WHERE route_id = ?";
        try {
            ResultSet rs = sql.executeSelectQuery(query, routeID);
            while (rs.next()) {

                int shuttle_id = rs.getInt("shuttle_id");
 
                shuttle = new Shuttle(shuttle_id);


            }
            sql.closeConnection();
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ShuttleTracker.class.getName()).log(Level.SEVERE, null, ex);
            shuttle = null;
        }
        
        return shuttle;
    }

    public ShuttleTracker.STATUS setStatus(String status) {
        switch (status) {
            case "active":
                return ShuttleTracker.STATUS.active;
            case "inactive":
                return ShuttleTracker.STATUS.inactive;
            case "full":
                return ShuttleTracker.STATUS.full;
            default:
                return ShuttleTracker.STATUS.unknown;
        }

    }

    public String toXMLString() {
        String xmlString = "<shuttles>";
        for (Shuttle shuttle : shuttles) {
            xmlString += shuttle.toXMLString();
        }
        xmlString += "</shuttles>";
        return xmlString;
    }

    public enum STATUS {

        active, inactive, full, unknown
    }

    public boolean setActiveShuttle(int routeID, int shuttleID) {
        String query = "INSERT INTO ActiveShuttles values (?,?)";
        boolean flag;
        try {
            flag = sql.executeUpdateQuery(query, shuttleID, routeID);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ShuttleTracker.class.getName()).log(Level.SEVERE, null, ex);
            flag = false;
        }
        return flag;
    }

    public boolean deleteActiveShuttle(int routeID, int shuttleID) {
        String query = "DELETE FROM ActiveShuttles WHERE route_id = ? AND shuttle_id = ?";
        boolean flag;
        try {
            flag = sql.executeUpdateQuery(query, routeID, shuttleID);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ShuttleTracker.class.getName()).log(Level.SEVERE, null, ex);
            flag = false;
        }
        return flag;
    }
    public void getActiveShuttles() {

    }

}
